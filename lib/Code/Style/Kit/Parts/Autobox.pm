package Code::Style::Kit::Parts::Autobox;
use strict;
use warnings;
# VERSION
# ABSTRACT: add autobox to your kit

=for :stopwords autobox

=head1 SYNOPSIS

  package My::Kit;
  use parent qw(Code::Style::Kit Code::Style::Kit::Parts::Autobox);
  1;

Then:

  package My::Module;
  use My::Kit;

  # you have autobox::Core, autobox::Camelize, autobox::Transform


=head1 DESCRIPTION

This part defines the feature C<autobox>, enabled by default, which
imports L<< C<autobox::Core> >>, L<< C<autobox::Camelize> >>, L<<
C<autobox::Transform> >>.

=cut

sub feature_autobox_default { 1 }
sub feature_autobox_export_list {
    qw(autobox::Core autobox::Camelize autobox::Transform);
}

1;
