package Code::Style::Kit::Parts::Moose;
use strict;
use warnings;
# VERSION
# ABSTRACT: Moose-based OO

=head1 SYNOPSIS

  package My::Kit;
  use parent qw(Code::Style::Kit Code::Style::Kit::Parts::Moose);
  1;

Then:

  package My::Class;
  use My::Kit 'class';

  # this is now a Moose class

  package My::Role;
  use My::Kit 'role';

  # this is now a Moose role

=head1 DESCRIPTION

This part defines the C<class> and C<role> features, which import L<<
C<Moose> >> and L<< C<Moose::Role> >> respectively. Class are made
immutable automatically.

If your kit defines a C<types> feature, it will be imported as well.

In addition, this part also defines the C<nonmoose> feature, to import
L<< C<MooseX::NonMoose> >>.

=cut

use Import::Into;
use Carp;
use Hook::AfterRuntime;

# Moose class
sub feature_class_export {
    my ($self, $caller) = @_;

    croak "can't be both a class and a role"
        if $self->is_feature_requested('role');

    require Moose;
    Moose->import({ into => $caller });

    after_runtime { $caller->meta->make_immutable };

    $self->maybe_also_export('types');
}
sub feature_class_order { 200 }

# extend non-moose classes
sub feature_nonmoose_export {
    require MooseX::NonMoose;
    MooseX::NonMoose->import({ into => $_[1] });
}
sub feature_nonmoose_order { 210 }

# Moose role
sub feature_role_export {
    my ($self, $caller) = @_;

    croak "can't be both a class and a role"
        if $self->is_feature_requested('class');

    require Moose::Role;
    Moose::Role->import({ into => $caller });

    $self->maybe_also_export('types');
}

1;
