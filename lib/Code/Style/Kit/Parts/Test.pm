package Code::Style::Kit::Parts::Test;
use strict;
use warnings;
# VERSION
# ABSTRACT: commonly used test modules (Test::More style)

=head1 SYNOPSIS

  package My::Kit;
  use parent qw(Code::Style::Kit Code::Style::Kit::Parts::Test);
  1;

Then:

  use My::Kit 'test';

  # write your test

=head1 DESCRIPTION

This part defines the C<test> feature, which imports L<< C<Test::More>
>>, L<< C<Test::Deep> >>, L<< C<Test::Fatal> >>, L<< C<Test::Warnings>
>>, adds F<t/lib> to C<@INC>, and sets up L<<
C<Log::Any::Adapter::TAP> >>.

=cut

use Import::Into;

sub feature_test_export {
    my ($self, $caller) = @_;

    require Test::More;
    Test::More->import::into($caller);
    require Test::Deep;
    Test::Deep->import::into($caller);
    require Test::Fatal;
    Test::Fatal->import::into($caller);
    require Test::Warnings;
    Test::Warnings->import::into($caller);
    require lib;
    lib->import::into($caller, 't/lib');
    require Log::Any::Adapter;
    # log to TAP, showing the category, skipping the "how to use
    # this" message, and showing all log messages
    $ENV{TAP_LOG_ORIGIN}=1;
    $ENV{TAP_LOG_SHOW_USAGE}=0;
    Log::Any::Adapter->set(
        TAP => ( filter => 'none' ),
    );
}

1;
