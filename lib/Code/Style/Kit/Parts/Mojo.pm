package Code::Style::Kit::Parts::Mojo;
use strict;
use warnings;
# VERSION
# ABSTRACT: Mojo-based OO

=head1 SYNOPSIS

  package My::Kit;
  use parent qw(Code::Style::Kit Code::Style::Kit::Parts::Mojo);
  1;

Then:

  package My::Class;
  use My::Kit class => [ 'My::Base::Class' ];

  # this is now a Mojo class, extending My::Base::Class

  package My::Role;
  use My::Kit 'role';

  # this is now a Mojo role

=head1 DESCRIPTION

This part defines the C<class> and C<role> features, which import L<<
C<Mojo::Base> >>.

=cut

use Import::Into;
use Carp;

sub feature_class_default { 0 }
sub feature_class_takes_arguments { 1 }
sub feature_class_export {
    my ($self, $caller, @arguments) = @_;

    croak "can't be both a class and a role"
        if $self->is_feature_requested('role');

    require Mojo::Base;
    Mojo::Base->import::into(
        $caller,
        @arguments ? ( $arguments[0] ) : ( '-base' ),
    );
}

sub feature_role_default { 0 }
sub feature_role_export {
    my ($self, $caller) = @_;

    croak "can't be both a class and a role"
        if $self->is_feature_requested('class');

    require Mojo::Base;
    Mojo::Base->import::into(
        $caller, '-role',
    );
}

1;
